<?php

namespace Task\GetOnBoard\Entity;

class Community
{
    public $id;
    public $name;
    public $posts = [];
    const ARTICLE_TYPE = 'article';
    const CONVERSATION_TYPE = 'conversation';
    const QUESTION_TYPE = 'question';

    public function __construct()
    {
        $this->id =  uniqid();
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }


    /**
     * @param $title
     * @param $text
     * @return Post|null
     */
    public function addPostArticle($title, $text)
    {
        $post = null;
        $post = new Post();
        $post->setTitle($title);
        $post->setText($text);
        $post->setType(self::ARTICLE_TYPE);

        $this->posts[] = $post;

        return $post;
    }

    /**
     * @param $text
     * @return Post|null
     */
    public function addPostConversation($text)
    {
        $post = null;
        $post = new Post();
        $post->setText($text);
        $post->setType(self::CONVERSATION_TYPE);

        $this->posts[] = $post;

        return $post;
    }

    /**
     * @param $title
     * @param $text
     * @return Post|null
     */
    public function addPostQuestion($title, $text)
    {
        $post = null;
        $post = new Post();
        $post->setTitle($title);
        $post->setText($text);
        $post->setType(self::QUESTION_TYPE);


        $this->posts[] = $post;

        return $post;
    }

    /**
     * @param $id
     * @param $title
     * @param $text
     * @return mixed|null
     */
    public function updatePost($id, $title, $text)
    {
        $post = null;
        foreach ($this->posts as &$post) {
            if ($post->id == $id) {
                $post->setTitle($title);
                $post->setText($text);
                break;
            }
        }

        return $post;
    }

    /**
     * @param $parentId
     * @param $text
     * @return null
     */
    public function addComment($parentId, $text)
    {
        $post = null;
        foreach ($this->posts as $post) {
            if ($post->id == $parentId) {
                break;
            }
        }


        $comment = $post->addComment($parentId, $text);

        return $comment;
    }

    /**
     * @param $id
     */
    public function deletePost($id)
    {
        $post = null;
        foreach ($this->posts as $post) {
            if ($post->id == $id) {
                break;
            }
        }

        $post->setDeleted(true);
    }

    /**
     * @return array
     */
    public function getPosts()
    {
        $posts = [];
        foreach ($this->posts as $post){
            if (!$post->getDeleted()) {
                $posts[] = array(
                 'post' => $post,
                 'username' => $post->getUser()->getUsername()
                );
            }
        }

        return $posts;
    }

    /**
     * @param $articleId
     * return void
     */
    public function disableCommentsForArticle($articleId): void
    {
        $post = null;
        foreach ($this->posts as $post) {
            if ($post->id == $articleId) {
                break;
            }
        }

        $post->setCommentsAllowed(false);
    }
}
