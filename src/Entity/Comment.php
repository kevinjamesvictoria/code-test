<?php

namespace Task\GetOnBoard\Entity;

class Comment
{
    public $id;
    public $text;
    public $parent;

    public function __construct()
    {
        $this->id =  uniqid();
    }

    public function getId()
    {
        return $this->id;
    }

    public function setText($text)
    {
        $this->text = $text;
    }

    public function getText(): string
    {
        return $this->text;
    }

    /**
     * @return mixed
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @param $parent
     */
    public function setParent($parent): void
    {
        $this->parent = $parent;
    }

}
